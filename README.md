EDGE Documentation
==================

Documentation for the EDGE bioinformatics project, a DTRA-funded NGS analysis effort.

* [Read The Docs](http://edge.readthedocs.org/)

* [PDF](https://readthedocs.org/projects/edge/downloads/pdf/v1.5/)

* [ebook](https://readthedocs.org/projects/edge/downloads/epub/v1.5/)

![ebook](https://bytebucket.org/nmrcjoe/edge-docs/raw/ebc26d88f1c4d6b11e96e2d1c3553873a4dd7cab/ebook.png)
